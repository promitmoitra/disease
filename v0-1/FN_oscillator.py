import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp
from scipy.optimize import fsolve

tmin=0;tmax=100;t=np.linspace(tmin,tmax,int(1e5))
impulse = 0.3 

def dv_dt(z,i):
    v,w=z
    return v-((v**3)/3) - 0.5*w  

def dw_dt(z,i):
    v,w=z
    return (2*(v) - i*(w)) + 1.3

def d_dt(t,z):
    v,w=z
    I = impulse if 20<t else 1
    return [dv_dt(z,1) , dw_dt(z,I)] 

def d0_dt(z):
    v,w=z
    return [dv_dt(z,1),dw_dt(z,1)]

if __name__=='__main__':
    z0=fsolve(d0_dt,[-1,-1])
    sol = solve_ivp(d_dt,[tmin,tmax],z0,dense_output=True)
    
    z=sol.sol(t)
    plt.plot(z[0],z[1])
    
    v=np.linspace(-5,5,int(1e3))
    w=np.linspace(-5,5,int(1e3))
    v,w=np.meshgrid(v,w)
    plt.contour(v,w,dv_dt((v,w),1),[0],colors='blue')
##    plt.contour(v,w,dv_dt((v,w),impulse),[0],colors='red')
    plt.contour(v,w,dw_dt((v,w),1),[0],colors='blue')
    plt.contour(v,w,dw_dt((v,w),impulse),[0],colors='red')

    plt.show()
