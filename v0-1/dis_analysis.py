import numpy as np
from matplotlib import colors
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import dis_CA as ca
import dis_core_ant as ant


def set_plot_text(pop,ax):
    for txt in ax.texts:
        txt.set_text('');
    core_len = int(len(pop))
    for i in range(core_len):
        for j in range(core_len):
            c='w' if 0<pop.T[i,j]<=cycle else 'k'
            s_txt = ax.text(i,j,'{0:2d}'.format(int(pop.T[i,j])),color=c,va='center',ha='center')
    return s_txt,

def plot(pop,cid=None,ax=None,cbar=False,text=False):
##    core_len = int(np.sqrt(len(pop)))+2
    core_len = int(len(pop))
    if ax == None:
        fig,ax = plt.subplots()
    ax.set_xticks(np.array(range(0,core_len))+0.5)
    ax.set_yticks(np.array(range(0,core_len))+0.5)
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.tick_params(axis='both', which='both', length=0)
    ax.set_aspect('equal')
    ax.grid(which='both')
    if cid:
        ax.set_title("$ID={}$".format(cid))
    if text:
        s_txt, = set_plot_text(pop,ax)
    img = ax.imshow(pop,cmap=cmap,norm=norm,interpolation='none')
    if cbar:
        if cbar == 'v':
            cbar = plt.colorbar(img,ax=ax,cmap=cmap,norm=norm,
                        boundaries=bounds,spacing='proportional',
                        ticks=range(num_states),orientation='vertical',
                        drawedges=False)
        elif cbar == 'h':
            cbar = plt.colorbar(img,ax=ax,cmap=cmap,norm=norm,
                        boundaries=bounds,spacing='proportional',
                        ticks=range(num_states),orientation='horizontal',
                        drawedges=False)
##        cbar.ax.tick_params(labelsize=20)
        fig.tight_layout()
        plt.show()
    return img,


def run_config_id(pair,core,config_id,T=50,anim=False,emb=True,p_bounds=False,radius=1):
    """

    """
    active = pair[0] ; passive = pair[1] ; n_states = active+passive+1
    str_state = ant.base_conv(config_id,n_states);str_state = str_state.rjust(core,'0')
    
    if emb:
        grid_size = np.sqrt(core)+2
        p = np.zeros((int(grid_size),int(grid_size)))
        q = ant.str_to_state(str_state,n_states)
        if int(np.sqrt(core))%2==0:
            p[int(grid_size/2)-int(np.sqrt(core)/2):int(grid_size/2)+int(np.sqrt(core)/2),
            int(grid_size/2)-int(np.sqrt(core)/2):int(grid_size/2)+int(np.sqrt(core)/2)]=q
        else:
            p[int(grid_size/2)-int(np.sqrt(core)/2):int(grid_size/2)+int(np.sqrt(core)/2)+1,
            int(grid_size/2)-int(np.sqrt(core)/2):int(grid_size/2)+int(np.sqrt(core)/2)+1]=q
    else:
        grid_size = np.sqrt(core)        
        p = ant.str_to_state(str_state,n_states)

    com = [ca.Population(p=np.copy(p),act=active,pas=passive,periodic=p_bounds,r=radius)]

    states=np.empty((T,int(grid_size),int(grid_size)),dtype=np.int8);str_states = []
    for t in range(T):
        states[t] = np.copy(np.array(com[0].p,dtype=np.int8))        
        state = ant.state_to_str(com[0].p,n_states)
        str_states.append(state)
        com = ca.run(com)
    if anim:
        def animate(n):
            img.set_data(states[n])
            ax.set_title('$t={}$'.format(n))
            text, = set_plot_text(states[n],ax)
            return img,text,
        
        fig,ax = plt.subplots()
        img, = plot(states[0],ax=ax);txt, = set_plot_text(states[0],ax)        
        cax = fig.add_axes([0.83,0.11,0.05,0.775])
##        cax = fig.add_axes([0.05,0.05,0.9,0.03])
        cbar = plt.colorbar(img,cax=cax,cmap=cmap,norm=norm,
                            boundaries=bounds,spacing='proportional',
                            ticks=range(num_states),
                            drawedges=False,orientation='vertical')        
        ani = animation.FuncAnimation(fig, animate, T,interval=500,repeat=False)
        plt.show()

    return states,p


def plot_distance(act,pas,core,idx):
    """

    """
    cycle = act + pas; num_states = cycle + 1
    states,config = run_config_id((active,passive),core,idx,emb=True)
    h_dist_init = [0] ; h_dist = [0]
    for i in range(1,len(states)):
        h_dist.append(ant.modulo(states[i-1],states[i],core,num_states))#(np.sqrt(core)+2)**2))     ##Distance from previous state
        h_dist_init.append(ant.modulo(states[0],states[i],core,num_states))#(np.sqrt(core)+2)**2))       ##Distance from initial state
    plt.xlim(-0.1,len(states));plt.ylim(-0.1,1.1)
    plt.xlabel("$Time\ -\ t$")
    plt.ylabel("$Hamming\ distance\ -\ D_H$")
    plt.title("$Core\ Size:\ {0},\ Cycle\ Length:\ {1},\ Config:\ {2}$".format(core,cycle,idx))
    plt.plot(range(len(states)),h_dist,'r.-',label="From prev config")
    plt.plot(range(len(states)),h_dist_init,'k.-',label="From init config")
    plt.legend(loc=4)
    plt.show()
    return h_dist,h_dist_init


def plot_perst_prob(cycle,core,flag,anchor=1,show=True):
    """
    
    """
    cycle_set = list(range(1,cycle))
    fixed_len_pairs = [(i,j) for i in cycle_set for j in cycle_set if i+j==cycle]
    fixed_passive = anchor ; inc_act_pairs = [(i,fixed_passive) for i in cycle_set]
    fixed_active = anchor ; inc_pas_pairs = [(fixed_active,i) for i in cycle_set]

    plt.ylim(-0.1,1.1)
    plt.ylabel("$P_{perst}$")

    if flag=='Fixed\ cycle':
        state_pair=fixed_len_pairs
        plt.xticks(range(len(state_pair)),labels=state_pair)
        plt.xlabel("$States\ -\ (Active,Passive)$")
        plt.title("$Core\ Size:\ {0},\ Cycle\ Length:\ {1}$".format(core,cycle))
    elif flag=='Fixed\ passive':
        state_pair=inc_act_pairs
        plt.xticks(range(len(state_pair)),labels=[i[0] for i in state_pair])
        plt.xlabel("$Active$")
        plt.title("$Core\ Size:\ {0}$".format(core))
        plt.plot(range(len(state_pair)),[0.6]*len(state_pair),'--k')
    elif flag=='Fixed\ active':
        state_pair=inc_pas_pairs
        plt.xticks(range(len(state_pair)),labels=[i[1] for i in state_pair])
        plt.xlabel("$Passive$")
        plt.title("$Core\ Size:\ {0}$".format(core))
    else:
        print("Incorrect flag!")

    act_frac = {}
    for pair in state_pair:
##        try:
        act_config_ids = np.load(ws_dir+"act_config_ids_states-({0:02d},{1:02d})_core-{2:02d}.npy".format(*pair,core))
##        act_config_ids = np.load(ant_dir+"act_config_ids_states-({0:02d},{1:02d})_core-{2:02d}.npy".format(*pair,core))
        frac = len(act_config_ids)/(sum(pair)+1)**core
        act_frac[pair]=frac
##        except FileNotFoundError:
##            print("Data for ({0},{1}) NOT available!".format(*pair))
##            act_frac[pair]=-1.0

    act_conf_frac = list(act_frac.values())
    plt.plot(range(len(state_pair)),act_conf_frac,'-o',label="${}$".format(anchor))
    if show:
        plt.show()
    return act_frac


def plot_basin(core):
    """
    Fraction of init configs that persist (for given core size) = basin[passive-1,active-1]

    """
    max_cyc = 15
    len_set = list(range(1,max_cyc))
    state_pairs=[(i,j) for i in len_set for j in len_set]#;state_pairs.pop(-1)
    basin = np.zeros((max_cyc-1,max_cyc-1))
    for pair in state_pairs:
        act = pair[0];pas=pair[1]
        conf_id = np.load(ws_dir+"act_config_ids_states-({0:02d},{1:02d})_core-{2:02d}.npy".format(act,pas,core))
##        conf_id = np.load(ant_dir+"act_config_ids_states-({0:02d},{1:02d})_core-{2:02d}.npy".format(act,pas,core))
        basin[pas-1,act-1] = len(conf_id)/(np.sum(pair)+1)**core
    plt.xticks(range(max_cyc-1),labels=len_set);plt.yticks(range(max_cyc-1),labels=len_set)
    plt.xlabel("$Active$");plt.ylabel("$Passive$")
    plt.imshow(basin,cmap='jet',origin='lower',interpolation='none')
    plt.title("$Core\ Size:\ {0}$".format(core))
    plt.colorbar()
    plt.show()
    return basin


def plotrange(init,fin,step,shape,rec):
    """

    """
##    core_len = int(np.sqrt(len(rec[0])))+2
    fig,ax = plt.subplots(*shape,figsize=(shape[1]*5,shape[0]*5))
    ctr=0
    for idx in range(init,fin,step):
        ID = list(act_config_dict.keys())[idx]
        img = plot(rec[idx],ID,ax=ax.flat[ctr],text='True')
        ctr+=1
##    cax = fig.add_axes([0.9,0.11,0.01,0.79])
    cax = fig.add_axes([0.05,0.05,0.9,0.03])
    cbar = plt.colorbar(img[0],cax=cax,cmap=cmap,norm=norm,
                        boundaries=bounds,spacing='proportional',
                        ticks=range(num_states),
                        drawedges=False,orientation='horizontal')
##    cbar.ax.tick_params(labelsize=20)
    plt.subplots_adjust(top=0.9,wspace=0.15,hspace=0.0)
    fig.suptitle("$Core:\ {}\ \ Cycle:\ ({},{})$".format(core,active,passive))
##    plt.savefig('{}.png'.format())
    fig.tight_layout()
    plt.show()
    return


if __name__=='__main__':
    import sys

    active=1; passive=1; cycle=active+passive; num_states=cycle+1
    core = np.square(2); idx = 23#int(sys.argv[1])
    cmap = colors.ListedColormap(['xkcd:pale grey','xkcd:darkish red','xkcd:almost black'])
    bounds = [0,0.5,active+0.5,cycle+0.5]
    norm = colors.BoundaryNorm(bounds,cmap.N)

    ant_dir = "./result/dis_core/core-{0:02d}/".format(core)
    ws_dir = "../antya_data/disease/result/dis_core/core-{0:02d}/".format(core)
    config_ids = np.load(ws_dir+"act_config_ids_states-({0:02d},{1:02d})_core-{2:02d}.npy".format(active,passive,core))
    print('Total configs =',num_states**core,'\nActive configs = ',len(config_ids))

##    act_config_dict={}
##    for cid in config_ids:
##        s,p=run_config_id((active,passive),core,cid)
##        act_config_dict[cid]=p

##    plot(act_config_dict[config_ids[0]],config_ids[0],cbar='h',text='True')
##    plotrange(0,46,1,(2,23),list(act_config_dict.values()))

    s,p=run_config_id((active,passive),core,idx,T=100,anim=True)
##    s,p=run_config_id((active,passive),core,idx,T=100,anim=True,emb=False)

##########!!!!Refine definition of hamming(a,b) in dis_core_ant!!!!##########    
    h_d,h_d_init = plot_distance(active,passive,core,idx)
#############################################################################

##    basin = plot_basin(core)
##    flag='Fixed\ cycle'
##    act_frac = plot_perst_prob(cycle,core,flag)
##
##    for i in range(1,cycle):
##        flag='Fixed\ active'
####        flag='Fixed\ passive'
##        active_fraction = plot_perst_prob(cycle,core,flag,i,show=False)
##        plt.legend(loc='best',title="${}$".format(flag))
##    plt.show()
