import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
from scipy.integrate import solve_ivp
from scipy.optimize import fsolve

tmin=0;tmax=200;dt=int(1e4);t=np.linspace(tmin,tmax,dt)

def I(t,peak_t,peak_h,sig):
    return peak_h*np.exp(-0.5*((t-peak_t)/sig)**2)

def dphi_dt(t,phi):
    sig1 = 2.0*np.heaviside(phi[0]%(2*np.pi),0.0)#10*np.sin(0.5*phi[0])*np.sin(0.5*phi[0])#(2.0+I(phi[0],np.pi/4,8,1))*np.heaviside(np.sin(phi[0]),0.0)
    sig2 = 2.0*np.heaviside(phi[1]%(2*np.pi),0.0)#10*np.sin(0.5*phi[1])*np.sin(0.5*phi[1])#(2.0+I(phi[1],np.pi/4,8,1))*np.heaviside(np.sin(phi[1]),0.0)
    phi1_dot = -np.sin(phi[0])+1.1#+sig2*np.heaviside(np.sin(phi[0]),1.0)
    phi2_dot = 1#-np.sin(phi[1])+sig1*np.heaviside(np.sin(phi[1]),1.0)
    return [phi1_dot,phi2_dot] 

##def dphi_dt_alt(phi,t):
##    I_t = 0.5*np.sin(t%(2*np.pi))+I_0#I(t,25,1,1)-I(t,50,2,9)+I_0
##    return 1-np.cos(phi%(2*np.pi)) + I_t*(1+np.cos(phi%(2*np.pi)))

def solve():
    phi0=[2.0,3.5]
    sol = solve_ivp(dphi_dt,[tmin,tmax],phi0,dense_output=True)
    phi=sol.sol(t)
    return phi

def init():
    title.set_text("$t={0:.1f}$".format(0.0))
##    fp_stable.set_data([], [])
##    fp_unstable.set_data([], [])
    line1.set_data([], [])
    line2.set_data([], [])
    return line1,line2,title,#fp_stable,fp_unstable,

def update(i):
    title.set_text("$t={0:.1f}$".format(t[i]))
##    phi0=fsolve(dphi_dt_alt,[-1,1],args=(t[i]))
##    fp_stable.set_data(np.cos(phi0[0]),np.sin(phi0[0]))
##    fp_unstable.set_data(np.cos(phi0[1]),np.sin(phi0[1]))
    line1.set_data(np.cos(phi1[i]),np.sin(phi1[i]))
    line2.set_data(np.cos(phi2[i]),np.sin(phi2[i]))
    return line1,line2,title,#fp_stable,fp_unstable,

if __name__=='__main__':

    phi1,phi2 = solve()

##    phi=np.reshape(phi.T,dt)

    fig,ax=plt.subplots()
    ax.set_xlim(-1.5,1.5);ax.set_ylim(-1.5,1.5);ax.set_aspect('equal')
    ax.plot(np.cos(np.linspace(0,2*np.pi,int(1e2))),
            np.sin(np.linspace(0,2*np.pi,int(1e2))),'k')

    title = ax.text(0.8,0.9,"$t={0:.1f}$".format(0),transform=ax.transAxes)
##    fp_stable, = ax.plot(np.cos(phi0[0]),np.sin(phi0[0]),'o',color='C0')
##    fp_unstable, = ax.plot(np.cos(phi0[1]),np.sin(phi0[1]),'o',color='C3')

    line1, = ax.plot(np.cos(phi1),np.sin(phi1),'o',color='C0')
    line2, = ax.plot(np.cos(phi2),np.sin(phi2),'o',color='C1')
    anim = animation.FuncAnimation(fig,update,init_func=init,
                               frames=len(t),interval=1,blit=True,repeat=False)

    plt.show()
