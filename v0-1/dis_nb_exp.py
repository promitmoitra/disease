import numpy as np
from matplotlib import pyplot as plt
import matplotlib.animation as anim
import dis_CA as ca

trans=10;obs=10;T=trans+obs
N = 4 

tau_i = 1 
tau_r = 1 
tau_0 = tau_i+tau_r

num_states = tau_0+1
num_nbrs = 2**2
##num_nbrs = N**2

hex_dict = {'a' : 10 , 'b' : 11 , 'c' : 12 , 'd' : 13 , 'e' : 14 , 'f' : 15}
inv_hex_dict = {v: k for k,v in hex_dict.items()}

def convert_number_system(input_number,input_base,output_base,length):

    #list that holds the numbers to output in the end
    remainder_list = []

    #start value for sum_base_10. All calculations go thorugh base-10.
    sum_base_10 = 0

    #reverse string to start calculating from least significant number
    reversed_input_number = input_number[::-1]

    #check for letters outside HEX range.
    for index, number in enumerate(reversed_input_number):
        for key,value in hex_dict.items():
            if str(number).lower() == key:
                number = value
        sum_base_10 += (int(number)*(int(input_base)**index))

    while sum_base_10 > 0:
        divided = sum_base_10// int(output_base)                        #find value for next iteration
        remainder_list.append(str(sum_base_10 % int(output_base)))      #find remainder to keep
        sum_base_10 = divided

    #fix the list and send a number:
    return_number = ''

    #hex output if output_base > 9:
    if output_base > 9:
        for index,each in enumerate(remainder_list):       #loop through remainder_list and convert 10+ to letters.
                if int(each) in inv_hex_dict.keys():
                    remainder_list[index] = inv_hex_dict[int(each)]

    #return the number as str:
    for each in remainder_list[::-1]:
        return_number += each
    
    return_number = return_number.rjust(length,'0') 
    return return_number


def str_to_state(str_config):
    pop = []
    for index,symbol in enumerate(str_config):
        if symbol in hex_dict.keys():
            pop.append(int(hex_dict[symbol]))
        else:    
            pop.append(int(symbol))
    pop = np.reshape(pop,(int(np.sqrt(len(str_config))),int(np.sqrt(len(str_config)))))
    return pop


def state_to_str(pop):
    str_config = ''
    pop = pop.flatten()
    for index,symbol in enumerate(pop):
        if symbol in inv_hex_dict.keys():
            str_config += str(inv_hex_dict[symbol])
        else:    
            str_config += str(int(symbol))
    return str_config


def hamming(str1,str2):
    i = 0
    count = 0
    while(i < len(str1)):
        if(str1[i] != str2[i]):
            count += 1
        i += 1
    return count/num_nbrs
    
if __name__=='__main__':
    import time
    start = time.monotonic()
    com = []

########==Specific init config==########
##    p = str_to_state('0201')

####==Embedded in larger grid==####
##    p = np.zeros((N,N))
####    q = str_to_state('011221021')
##    q = str_to_state('1021')
##    if int(np.sqrt(num_nbrs))%2==0:
##        p[int(N/2)-int(np.sqrt(num_nbrs)/2):int(N/2)+int(np.sqrt(num_nbrs)/2),
##        int(N/2)-int(np.sqrt(num_nbrs)/2):int(N/2)+int(np.sqrt(num_nbrs)/2)]=q
##    else:
##        p[int(N/2)-int(np.sqrt(num_nbrs)/2):int(N/2)+int(np.sqrt(num_nbrs)/2)+1,
##        int(N/2)-int(np.sqrt(num_nbrs)/2):int(N/2)+int(np.sqrt(num_nbrs)/2)+1]=q
########################################

########==Random init config==########
##    s_str = ''
##    s = np.random.choice(range(num_states),size=num_nbrs)
##    for i in s:
##        s_str += str(i)
##    p = str_to_state(s_str)
######################################

##    com.append(ca.Population(p=p,act=tau_i,pas=tau_r))
##
##    fig,ax = plt.subplots()
##    ani = anim.FuncAnimation(fig, ca.animate, T, fargs=(com,(ax)),interval=5000,repeat=False)
##    plt.show()

##    states = []
##    for t in range(T):
##        state = state_to_str(com[0].p)
##        states.append(state)
##        com = ca.run(com)

##    h_dist = []
##    for i in range(len(states)):
##        h_dist.append(hamming(states[i-1],states[i])) 
##    plt.xlim(-0.1,T);plt.ylim(0,1.1)
##    plt.plot(range(T),h_dist);plt.show()       

########==All initial configs (all num_nbrs digit, num_states-ary numbers)==########
##    for ic in range(num_states**num_nbrs):
##        s_str = convert_number_system(str(ic),10,num_states,num_nbrs)
######==Embedded in larger grid==####
##        p = np.zeros((N,N))
##        q = str_to_state(s_str)
##        if int(np.sqrt(num_nbrs))%2==0:
##            p[int(N/2)-int(np.sqrt(num_nbrs)/2):int(N/2)+int(np.sqrt(num_nbrs)/2),
##            int(N/2)-int(np.sqrt(num_nbrs)/2):int(N/2)+int(np.sqrt(num_nbrs)/2)]=q
##        else:
##            p[int(N/2)-int(np.sqrt(num_nbrs)/2):int(N/2)+int(np.sqrt(num_nbrs)/2)+1,
##            int(N/2)-int(np.sqrt(num_nbrs)/2):int(N/2)+int(np.sqrt(num_nbrs)/2)+1]=q
##        com.append(ca.Population(p=p,act=tau_i,pas=tau_r))
##
##    for t in range(T):
##        com = ca.run(com)
##
##    check_act = []
##    for i in com:
##        state = state_to_str(i.p)
##        if any(str(e) in state for e in range(1,tau_i+1)):
##            check_act.append(1)
##        else:
##            check_act.append(0)
##
##    act_config_num = [i for i,e in enumerate(check_act) if e==1]
##    act_configs = [convert_number_system(str(i),10,num_states,num_nbrs) for i in act_config_num]
##    print(len(act_config_num)/num_states**num_nbrs)
##    
##    end=time.monotonic()
##    print(time.strftime("%H:%M:%S",time.gmtime(end-start)))
####################################################################################

    def check_active_configs(active,passive,core_size):
        n_states = active+passive+1
        grid_size = np.sqrt(core_size)+2
        com = []

        for ic in range(n_states**core_size):
            s_str = convert_number_system(str(ic),10,n_states,core_size)
            p = np.zeros((int(grid_size),int(grid_size)))
            q = str_to_state(s_str)
            if int(np.sqrt(core_size))%2==0:
                p[int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2),
                int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)]=q
            else:
                p[int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)+1,
                int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)+1]=q

            com.append(ca.Population(p=p,act=active,pas=passive))
    
        for t in range(T):
            com = ca.run(com)
    
        check_act = []
        for i in com:
            state = state_to_str(i.p)
            if any(str(e) in state for e in range(1,active+1)):
                check_act.append(1)
            else:
                check_act.append(0)
    
        act_config_num = [i for i,e in enumerate(check_act) if e==1]
##        act_configs = [convert_number_system(str(i),10,n_states,core_size) for i in act_config_num]
        active_config_fraction = (len(act_config_num)/n_states**core_size)
        return active_config_fraction

    def plot_active_configs_prob(state_pair,core_len):
        act_conf_frac = []
        for pair in state_pair:
            act_conf_frac.append(check_active_configs(*pair,core_len**2))
            print(pair,'Done!')
        plt.plot(range(len(state_pair)),act_conf_frac,'-o')
        plt.xticks(range(len(state_pair)),[str(i) for i in state_pair],size='small')
        plt.xlabel("$States\ -\ (Active,Passive)$")
        plt.ylabel("$P_{perst}$")
        plt.title("$Core\ Size:\ {0},\ Cycle\ Length:\ {1}$".format(core_len**2,state_cycle))
        plt.show()
        return

    state_cycle = 10
    state_set = list(range(state_cycle))
    fixed_len_state_pair = [(i,j) for i in state_set for j in state_set if i+j==state_cycle]
    fixed_passive = 5;dom_act_state_pair = [(i,fixed_passive) for i in state_set[1:]]
    fixed_active = 2; dom_pas_state_pair = [(fixed_active,i) for i in state_set[1:]]

##    plot_active_configs_prob(fixed_len_state_pair,2)
    plot_active_configs_prob(dom_act_state_pair,2)
##    plot_active_configs_prob(dom_pas_state_pair,2)

    end=time.monotonic()
    print(time.strftime("%H:%M:%S",time.gmtime(end-start)))
