import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
import matplotlib.animation as anim
import random

##N = 50
trans=100;obs=100;T=trans+obs

tau_i = 5 
tau_r = 10
tau_0 = tau_i+tau_r
N = tau_0

class Population:
    def __init__(self,i_0=None,r_0=None,p=None,size=None,act=None,pas=None,r=1,periodic=False):

        if act is None:
            self.act=tau_i
        else:
            self.act=act

        if pas is None:
            self.pas=tau_r
        else:
            self.pas=pas
        
        self.tau0=self.act+self.pas
        self.loc = []
        self.scount,self.icount,self.rcount = 0,0,0
        self.stime,self.itime,self.rtime = [],[],[]

        if p is None:
        
            if size is None:
                self.size = N
            else:
                self.size = size

            if i_0 is None:
                self.i_0 = 0.1
            else:
                self.i_0 = i_0

            if r_0 is None:
                self.r_0 = 0.5*(1-i_0)
            else:
                self.r_0 = r_0

##            self.p = np.random.randint(1,self.tau0+1,size=self.size*self.size)
            self.p = np.zeros(self.size*self.size)
            endi0 = int(self.i_0*(self.size**2))
            endr0 = endi0+int((self.r_0*(1-self.i_0))*(self.size**2))
            self.p[0:endi0] = 1
            self.p[endi0:endr0] = self.act+1
##            self.p[0:endi0] = np.random.randint(1,self.act+1)
##            self.p[endi0:endr0] = np.random.randint(self.act+1,self.tau0+1)

            np.random.shuffle(self.p)
            self.p = self.p.reshape(self.size,self.size)
        else:
            self.p = p
            self.size = len(p[0])
            self.count()
            self.i_0 = self.icount/self.size**2
            self.r_0 = self.rcount/self.size**2
        self.periodic = periodic
        self.r = r
        self.default_nbh = [np.array([x,y]) for x in range(-self.size+1,self.size)
                            for y in range(-self.size+1,self.size)
                            if np.sqrt(x**2+y**2)<=r and [x,y]!=[0,0]]

    def count(self):
        self.scount,self.icount,self.rcount = 0,0,0
        for i in range(self.size):
            for j in range(self.size):
                if self.p[i,j] == 0:
                    self.scount += 1
                elif self.p[i,j] in range(1,self.act+1):
                    self.icount += 1
                elif self.p[i,j] in range(self.act+1,self.act+self.pas+1):
                    self.rcount += 1
        self.stime.append(self.scount/self.size**2)
        self.itime.append(self.icount/self.size**2)
        self.rtime.append(self.rcount/self.size**2)
        return

    def nbr(self,i,j):
        if self.periodic:
            nbh = np.array([i,j])+self.default_nbh
            c_nbrs = []
            for i in nbh%self.size:
                c_nbrs.append(self.p[tuple(i)])
        else:
            c_nbh = np.array([i,j]) + self.default_nbh
            real_nbh = []
            for i in c_nbh:
                if 0<=i[0]<self.size and 0<=i[1]<self.size:
                    real_nbh.append(i)
            c_nbrs = []
            for i in real_nbh:
                c_nbrs.append(self.p[tuple(i)])
        return c_nbrs

    def check(self,loc):
        self.loc = []
        nbhood = []
        for i in range(self.size):
            for j in range(self.size):
                if self.p[i,j] == 0:
                    nbhood = self.nbr(i,j)
                    for k in nbhood:
                        if k in range(1,self.act+1):
                            self.loc.append([i,j])
                            break
        return self.loc

    def infect(self,loc):
        for i in range(self.size):
            for j in range(self.size):
                if self.p[i,j] >= self.tau0:
                    self.p[i,j] = 0
                if 1<=self.p[i,j]<self.tau0:
                    self.p[i,j] += 1
                if [i,j] in loc:
                    self.p[i,j] += 1
        return
####################################################################################

def run(com):
    for i in com:
        i.count()
    for i in com:
        i.loc = i.check(i.loc)
    for i in com:
        i.infect(i.loc)
    return com

#######Define discreet colormap
##c_i = plt.cm.hot_r(np.linspace(0,1,tau_i+1))
##c_r = plt.cm.copper(np.linspace(0,1,tau_r))
##c = np.vstack((c_i,c_r))
##cmap = colors.ListedColormap(c)
##b=np.arange(0.,tau_0+1,1);b[1:]-=0.01;bounds=b
##norm = colors.BoundaryNorm(bounds,cmap.N)

##cmap = colors.ListedColormap(['xkcd:pale grey','xkcd:darkish red','xkcd:almost black'])
##bounds = [0,0.99,tau_i+0.99,tau_0+0.99]
##norm = colors.BoundaryNorm(bounds,cmap.N)

def animate(n, com, ax):
    cmap = colors.ListedColormap(['xkcd:pale grey','xkcd:darkish red','xkcd:almost black'])
    bounds = [0,0.99,com[0].act+0.99,com[0].tau0+0.99]
    norm = colors.BoundaryNorm(bounds,cmap.N)
    ctr = 0
    ax.cla()
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_aspect('equal')
    ax.set_title("$t={}$".format(n+1))
    for i in range(com[0].size):
        for j in range(com[0].size):
            c='w' if com[ctr].p.T[i,j]>com[0].act else 'k'
            ax.text(i,j,'{0:1d}'.format(int(com[ctr].p.T[i,j])),color=c,va='center',ha='center')
    ax.imshow(com[ctr].p,cmap=cmap,norm=norm,interpolation='none')#,origin='lower')
##    ax.text(0.25,-0.05,"$S_t:{0:0>4d}$ | $I_t:{1:0>4d}$ | $R_t:{2:0>4d}$".format(
##       com[ctr].scount,com[ctr].icount,com[ctr].rcount),fontsize=10,transform=ax.transAxes)
    com = run(com)
##    print(com[ctr].p[24:27,24:27])#,com[ctr].p[25,26])
    plt.draw()
    return ax

if __name__=='__main__':
    com=[Population(i_0=0.0,r_0=0.0)]

    def icgen():
##        ic=np.zeros((tau_0,tau_0))
        ic=np.zeros((N,N))

        def set_patch(rl,rh,cl,ch,v):
            for i in range(rl,rh):
                for j in range(cl,ch):
                    ic[i,j]=v#np.random.randint(v[0],v[1])
        
        def set_row(r,cl,ch,v):
            for i in range(cl,ch):
                ic[r,cl:ch]=v
    
        def set_col(rl,rh,c,v):
            for i in range(rl,rh):
                ic[rl:rh,c]=v
    
##        set_patch(5,10,5,10,3)
        set_patch(4,7,4,7,15)
##        set_patch(0,tau_i,0,tau_i,(1,tau_i+1))
##        set_patch(0,tau_r,tau_i,tau_0,(tau_i+1,tau_0+1))

##        set_col(0,tau_i,int(tau_0/2)-1,range(1,tau_i+1))
##        set_col(0,tau_i,int(tau_0/2)-1,range(tau_i,0,-1))
##        set_col(0,tau_r,int(tau_0/2),range(tau_i+1,tau_0+1))
##        set_col(0,tau_r,int(tau_0/2),range(tau_0,tau_i,-1))

##        for i in range(tau_i):                                      ##Set downgrad active patch:
##            set_row(i,i,tau_i,range(1,tau_i+1-i))                       ##-Radially out left
##            set_row(i,i,tau_i,range(i+1,tau_i+1))                       ##-Left
##            set_row(i,i,tau_i,tau_i-i)                                  ##-Up

##        for i in range(tau_i):                                      ##Set upgrad active patch:
##            set_row(i,i,tau_i,range(tau_i,i,-1))                        ##-Radially out left
##            set_row(i,i,tau_i,range(tau_i-i,0,-1))                      ##-Left
##            set_row(i,i,tau_i,i+1)                                      ##-Up

##        for i in range(tau_r):                                      ##Set downgrad passive patch:
##            set_row(i,tau_i,tau_0-i,range(tau_0-i,tau_i,-1))            ##-Radially out right
##            set_row(i,tau_i,tau_0-i,range(tau_0,tau_i+i,-1))            ##-Right    
##            set_row(i,tau_i,tau_0-i,tau_0-i)                            ##-Up    

##        for i in range(tau_r):                                      ##Set upgrad passive patch:
##            set_row(i,tau_i,tau_0-i,range(tau_i+1+i,tau_0+1))           ##-Radially out right
##            set_row(i,tau_i,tau_0-i,range(tau_i+1,tau_0+1-i))           ##-Right
##            set_row(i,tau_i,tau_0-i,tau_i+1+i)                          ##-Up

################################################
    
##        for i in range(tau_i):                                      ##Set downgrad active patch:
##            set_row(i,tau_r-1,tau_0-1-i,range(1,tau_i+1-i))             ##-Radially out left
##            set_row(i,tau_r-1,tau_0-1-i,range(i+1,tau_i+1))             ##-Left
##            set_row(i,tau_r-1,tau_0-1-i,tau_i-i)                        ##-Up

##        for i in range(tau_i):                                      ##Set upgrad active patch:
##            set_row(i,tau_r-1,tau_0-1-i,range(tau_i,i,-1))              ##-Radially out left
##            set_row(i,tau_r-1,tau_0-1-i,range(tau_i-i,0,-1))            ##-Left
##            set_row(i,tau_r-1,tau_0-1-i,i+1)                            ##-Up
##            set_row(i,tau_r-1,tau_0-1-i,range(1,tau_i+1-i))
####            set_row(i,tau_r-1,tau_0-1-i,range(i+1,tau_i+1))

##        for i in range(tau_r):
##            set_col(i,tau_r,tau_0-1-i,range(tau_i+1+i,tau_0+1))
##            set_col(i,tau_r,tau_0-1-i,range(tau_i+1,tau_0+1-i))
    
    ##########==BALANCED ONLY==########
##        for i in range(tau_i):
##            set_row(i,i,tau_i,range(tau_i,i,-1))
##            set_row(i,tau_i-1,tau_0-1-i,range(i+1,tau_i+1)[:tau_r+1])
##        for i in range(tau_r):
##            set_col(i,tau_r,tau_0-1-i,range(tau_0,tau_r+i,-1))
##            set_col(tau_r-1,tau_0-1-i,tau_0-1-i,range(tau_r+1+i,tau_0+1))
    ###################################
    
##        for i in range(tau_i):
##            set_row(i,i,tau_i,range(tau_i,i,-1))
##            np.put(ic[i],sorted(list(set(min(tau_i-1+j,tau_0-1) for j in range(tau_i-i)))),
##                   range(i+1,tau_i+1)[:tau_r+1])
##        for i in range(tau_r):
##            np.put(ic.T[tau_0-1-i],list(range(tau_0-2-i,tau_i-2,-1)),
##                   range(tau_0,tau_i+i,-1))
##            np.put(ic.T[tau_0-1-i],sorted(list(set(max(tau_i-1-j,0) for j in range(tau_r-i))),reverse=True),
##                   range(tau_i+1+i,tau_0+1)[:tau_i])
        return ic

##########==SANDBOX==##########
##    for i in range(tau_i):
##        set_row(i,i,tau_i,range(tau_i,i,-1))
##        set_row(i,tau_r-1,tau_0-1-i,range(i+1,tau_i+1))
##
##    for i in range(tau_r):
##        set_col(i,tau_r,tau_0-1-i,range(tau_0,tau_r+i,-1))
##        set_col(i,tau_r,i,range(tau_0,tau_r+i,-1))
####        set_col(tau_r,tau_0-1-i,tau_0-1-i,range(tau_0+1,tau_i,-1))    
####        set_row(i,tau_i,tau_0-i,range(tau_i+1,tau_0+1-i))
    
##    set_patch(24,27,24,27,4)
##    set_patch(23,28,24,26,25)
##    set_patch(23,28,26,28,23)
##    com[0].p[25,25]=2;com[0].p[25,26:30]=[22,23,24,25]
##    print(com[0].p[24:27,24:28])

    spatch=icgen()
    com[0].p[:,:]=spatch
    com[0].p[5,5]=4;com[0].p[5,7]=14

##    com[0].p[int(N/2):int(N/2)+tau_0,int(N/2)-tau_0:int(N/2)]=np.copy(spatch)
##    com[0].p[int(N/2)-tau_0:int(N/2),int(N/2)-tau_0:int(N/2)]=np.copy(np.flipud(spatch))
##    com[0].p[int(N/2)-tau_0:int(N/2),int(N/2):int(N/2)+tau_0]=np.copy(np.fliplr(np.flipud(spatch)))
##    com[0].p[int(N/2):int(N/2)+tau_0,int(N/2):int(N/2)+tau_0]=np.copy(np.flipud(np.fliplr(np.flipud(spatch))))

##    com[0].p[int(N/2):int(N/2)+tau_0,int(N/2)-tau_0:int(N/2)]=np.copy(spatch)
##    com[0].p[int(N/2)-tau_0:int(N/2),int(N/2)-tau_0:int(N/2)]=np.copy(np.rot90(spatch,3))
##    com[0].p[int(N/2)-tau_0:int(N/2),int(N/2):int(N/2)+tau_0]=np.copy(np.rot90(spatch,2))
##    com[0].p[int(N/2):int(N/2)+tau_0,int(N/2):int(N/2)+tau_0]=np.copy(np.rot90(spatch))

##    com[0].p[49,40:50]=6;com[0].p[50:70,39]=6    

###############################

####    ind=[(x,y) for x,y in zip(range(tau_0-1,tau_i,-1),range(tau_i))]
##    ind=[(x,y) for x,y in zip(range(13,12,-1),range(5,6))]
##    ind+=[(x,y) for x,y in zip(range(13,10,-1),range(5,4,-1))]
##    for i in ind:
##        com[0].p[i]=tau_i+1
####        com[0].p[i[0],i[1]+1]=1
##    com[0].p[13,8]=1

    fig,ax = plt.subplots()
    ani = anim.FuncAnimation(fig, animate, T, fargs=(com,(ax)),interval=100,repeat=False)
    plt.show()

##    rec=np.zeros((T,N,N))
##    for t in range(T):
##        rec[t]=np.copy(com[0].p)
##        com=run()
##    np.save('./data.npy',rec)

###############################
##    import time
##    start=time.monotonic()
##    com=[]
##    for i in range(100):
##        com.append(Population(i_0=0.01))
##    for t in range(T):
##        com=run(com)
##    end=time.monotonic()
##    print(time.strftime("%H:%M:%S",time.gmtime(end-start)))
####    infected=[i.icount for i in com]
##    avg_inf = np.mean([np.mean(i.itime[trans:]) for i in com])
##    print(avg_inf)
