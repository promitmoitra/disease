import os,sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
import matplotlib.animation as anim
import random

C = 2
N = 10
T = 30
i_0 = 0.1
mig_fraction = 0.02

tau_i = 4
tau_r = 9

class Population:
    def __init__(self,pop_id,miglist=None,p=None,
                 size=N,i_0=i_0):
        self.pop_id = pop_id
        self.size = size
        self.i_0 = i_0
        self.loc = []
        self.scount,self.icount,self.rcount = 0,0,0
        self.stime,self.itime,self.rtime = [],[],[]

        if p is None:
            self.p = np.zeros(self.size*self.size)
            self.p[0:int(self.i_0*(self.size**2))] = 1#np.random.randint(1,5)
            for i in range(int(self.i_0*self.size**2),int(self.size**2)):
                switch = np.random.randint(0,100)
                if switch >= 50:   #switch >= 33,50,66 gives S:R = 1:2,1:1,2:1
                    self.p[i] = 5#np.random.randint(5,14)
            np.random.shuffle(self.p)
            self.p = self.p.reshape(self.size,self.size)
        else:
            self.p = p

        if miglist is None: 
            self.miglist=[]
            for i in range(self.size):
                for j in range(self.size):
                    switch = np.random.randint(0,100)
                    if switch <= mig_fraction*100:
                        if len(self.miglist)>=mig_fraction*(self.size**2):
                            pass
                        else: self.miglist.append([i,j])
            np.random.shuffle(self.miglist)
        else:
            self.miglist = miglist
            
    def state(self,ij):
        return self.p[ij[0],ij[1]]

    def count(self):
        self.scount,self.icount,self.rcount = 0,0,0
##        dist = 0
        for i in range(self.size):
            for j in range(self.size):
##                dist += abs(init[n,m] - p[n,m])
                if self.p[i,j] == 0:
                    self.scount += 1
                elif self.p[i,j] in range(1,tau_i+1):
                    self.icount += 1
                elif self.p[i,j] in range(tau_i+1,tau_i+tau_r+1):
                    self.rcount += 1
        self.stime.append(self.scount/self.size**2),self.itime.append(self.icount/self.size**2),self.rtime.append(self.rcount/self.size**2)
        return
    
    def nbr_list(self,i,j):

        leftbnd = [[x,0] for x in range(1,self.size-1)]
        rightbnd = [[x,self.size-1] for x in range(1,self.size-1)]
        topbnd = [[0,x] for x in range(1,self.size-1)]
        btmbnd = [[self.size-1,x] for x in range(1,self.size-1)]
        boundary = leftbnd+rightbnd+topbnd+btmbnd

        if [i,j] == [0,0]:
            def_nbrs = [self.p[i,j+1],self.p[i+1,j]]
            if [i,j] in self.miglist:
                idx = self.miglist.index([i,j])
                nbrs = get_nbrs(self.pop_id,i,j,idx,def_nbrs)
                return nbrs
            else:
                return def_nbrs

        elif [i,j] == [0,self.size-1]:
            def_nbrs = [self.p[i+1,j],self.p[i,j-1]]
            if [i,j] in self.miglist:
                idx = self.miglist.index([i,j])
                nbrs = get_nbrs(self.pop_id,i,j,idx,def_nbrs)
                return nbrs
            else:
                return def_nbrs

        elif [i,j] == [self.size-1,0]:
            def_nbrs = [self.p[i,j+1],self.p[i-1,j]]
            if [i,j] in self.miglist:
                idx = self.miglist.index([i,j])
                nbrs = get_nbrs(self.pop_id,i,j,idx,def_nbrs)
                return nbrs
            else:
                return def_nbrs

        elif [i,j] == [self.size-1,self.size-1]:
            def_nbrs = [self.p[i,j-1],self.p[i-1,j]]
            if [i,j] in self.miglist:
                idx = self.miglist.index([i,j])
                nbrs = get_nbrs(self.pop_id,i,j,idx,def_nbrs)
                return nbrs
            else:
                return def_nbrs

        elif [i,j] in boundary:
            if [i,j] in leftbnd:
                def_nbrs = [self.p[i-1,j],self.p[i,j+1],self.p[i+1,j]]
                if [i,j] in self.miglist:
                    idx = self.miglist.index([i,j])
                    nbrs = get_nbrs(self.pop_id,i,j,idx,def_nbrs)
                    return nbrs
                else:
                    return def_nbrs

            elif [i,j] in rightbnd:
                def_nbrs = [self.p[i-1,j],self.p[i,j-1],self.p[i+1,j]]
                if [i,j] in self.miglist:
                    idx = self.miglist.index([i,j])
                    nbrs = get_nbrs(self.pop_id,i,j,idx,def_nbrs)
                    return nbrs
                else:
                    return def_nbrs

            elif [i,j] in topbnd:
                def_nbrs = [self.p[i+1,j],self.p[i,j-1],self.p[i,j+1]]
                if [i,j] in self.miglist:
                    idx = self.miglist.index([i,j])
                    nbrs = get_nbrs(self.pop_id,i,j,idx,def_nbrs)
                    return nbrs
                else:
                    return def_nbrs

            elif [i,j] in btmbnd:
                def_nbrs = [self.p[i,j+1],self.p[i,j-1],self.p[i-1,j]]
                if [i,j] in self.miglist:
                    idx = self.miglist.index([i,j])
                    nbrs = get_nbrs(self.pop_id,i,j,idx,def_nbrs)
                    return nbrs
                else:
                    return def_nbrs

        elif [i,j] in self.miglist:
            def_nbrs = [self.p[i,j+1],self.p[i-1,j],self.p[i,j-1],self.p[i+1,j]]
            idx = self.miglist.index([i,j])
            nbrs = get_nbrs(self.pop_id,i,j,idx,def_nbrs)
            return nbrs

        else:
            def_nbrs = [self.p[i,j+1],self.p[i-1,j],self.p[i,j-1],self.p[i+1,j]]
            return def_nbrs

    def check(self,loc):
        self.loc = []
        nbhood = []
        for i in range(self.size):
            for j in range(self.size):
                if self.p[i,j] == 0:
                    nbhood = self.nbr_list(i,j)
                    for k in nbhood:
                        if k in range(1,tau_i+1):
                            self.loc.append([i,j])
                            break
        return self.loc

    def infect(self,loc):
##        loc = self.check()
        for i in range(self.size):
            for j in range(self.size):
                if self.p[i,j] != 0:
                    self.p[i,j] += 1
                    if self.p[i,j] >= tau_i+tau_r+1:
                        self.p[i,j] = 0
        for i in loc:
            self.p[i[0],i[1]] +=1
        return

############################################################################

def get_nbrs(pid,i,j,idx,def_nbrs):
    nbrs = list(def_nbrs)
    nbrs.pop(np.random.randint(len(nbrs)))
    if idx <= len(com[pid].miglist)/2:
        nbrs.append(com[(pid-1)%C].state(com[(pid-1)%C].miglist[idx]))
    else:
        nbrs.append(com[(pid+1)%C].state(com[(pid+1)%C].miglist[idx]))
    return nbrs

def run():
    for i in com:
        i.count()
    for i in com:
        i.loc = i.check(i.loc)
    for i in com:
        i.infect(i.loc)
    return com

##Define discreet colormap
cmap = colors.ListedColormap(['xkcd:pale grey','xkcd:darkish red','xkcd:almost black'])
bounds = [0,0.7,tau_i+0.3,tau_i+tau_r]
norm = colors.BoundaryNorm(bounds,cmap.N)

def animate(n, com, ax):
    ##    for i in com:
    ##        i.count()
    ##        print(i.pop_id,i.scount,i.stime)
    ##        print(n,len(i.stime))
    for i in ax:
        ctr = list(ax).index(i)
        i.cla()
        i.set_xticks([])
        i.set_yticks([])
        i.set_aspect('equal')
        i.set_title("$t={}$".format(n))
        i.imshow(com[ctr].p,origin='lower',cmap=cmap,norm=norm,interpolation='none')
        i.text(-2,-15,"$S_t:{0:0>4d}$ | $I_t:{1:0>4d}$ | $R_t:{2:0>4d}$".format(
            com[ctr].scount,com[ctr].icount,com[ctr].rcount),fontsize=10)
        i.text(N/2,-7,"${}$".format(com[ctr].pop_id))            
    com = run()
    plt.draw()
    return ax

##########################################################################

##com = [Population(c) for c in range(C)]
    
##p0=np.zeros((N,N))
##p0[25,25] = 1
##p1=np.zeros((N,N))

leftb = [[x,0] for x in range(0,N)]
rightb = [[x,N-1] for x in range(0,N)]
miglen = mig_fraction*N
miglist0 = random.sample(rightb,int(miglen))
##miglist1l = random.sample(leftb,int(miglen/2))
##miglist1r = random.sample(rightb,int(miglen/2))
miglist1 = random.sample(leftb,int(miglen))#miglist1l+miglist1r
##miglist2 = random.sample(leftb,int(miglen))

com = [Population(0,miglist=miglist0,i_0=0.1),
       Population(1,miglist=miglist1,i_0=0.1)]#,
##       Population(1,miglist=miglist2)]

##com = [Population(0,miglist=rightb,i_0=0.1),
##       Population(1,miglist=leftb,i_0=0.9)]

######Equalize miglists:
l=[]
for i in com:
    l.append(len(i.miglist))

for i in com:
    while len(i.miglist)-min(l):
        i.miglist.pop()

#########################################################################
if __name__=='__main__':
##    t=0
##    for i in com:
##        np.save('./plotter/st/i0-1-{0:1.2f}/t-{1:03d}_{2:1d}.npy'.format(com[1].i_0,t,i.pop_id),i.p)
    for t in range(T):
        com = run()
##        os.makedirs('./plotter/st/m-{0:1.2f}_i0-{1:1.2f}'.format(mig_fraction,com[1].i_0),exist_ok=True)
##        for i in com:
##            np.save('./plotter/st/m-{0:1.2f}_i0-{1:1.2f}/t-{2:03d}_{3:1d}.npy'.format(mig_fraction,com[1].i_0,t,i.pop_id),i.p)
##    np.save('./result/itref_m-{0:1.2f}_i0-0-{1:1.2f}.npy'.format(mig_fraction,com[0].i_0),com[0].itime)
##    np.save('./result/itref_m-{0:1.2f}_i0-1-{1:1.2f}.npy'.format(mig_fraction,com[1].i_0),com[1].itime)
##    fig,ax = plt.subplots()
##    for i in com:
##        if i.pop_id == 0:
##            ls='-'
##        elif i.pop_id == 1:
##            ls='--'
##        tseries = [i.stime,i.itime,i.rtime]
##        for ts in tseries:
##            if ts==tseries[0]:
##                c='gray'
##                label="$S_{{t}}^{0}$".format(i.pop_id)
##            elif ts==tseries[1]:
##                c='red'
##                label="$I_{{t}}^{0}$".format(i.pop_id)
##            elif ts==tseries[2]:
##                c='k'
##                label="$R_{{t}}^{0}$".format(i.pop_id)
##            ax.plot(range(T),ts,c=c,ls=ls,label=label)
##            
##    plt.legend()
##    plt.show()

    fig,ax = plt.subplots(1,C,sharex='all',sharey='all',squeeze=True)
    ##fig,ax = plt.subplots(2,2,sharex='all',sharey='all',squeeze=True)
    ani = anim.FuncAnimation(fig, animate, T, fargs=(com,ax),interval=100,repeat=False)
    plt.show()
