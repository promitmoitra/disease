import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
import matplotlib.animation as anim
import dis_CA as dis

rec=np.load('./data.npy')

N = int(dis.N)
T = 100#int(dis.T)
tau_i = int(dis.tau_i)
tau_r = int(dis.tau_r)
tau_0 = tau_i+tau_r
##N = tau_0

def nbr(data,i,j):
    halo_rec=np.zeros((N+2,N+2))
    lbnd = [(x,0) for x in range(0,N+2)]
    rbnd = [(x,N+1) for x in range(0,N+2)]
    tbnd = [(0,x) for x in range(0,N+2)]
    bbnd = [(N+1,x) for x in range(0,N+2)]
    boundary = lbnd+rbnd+tbnd+bbnd
    for idx in boundary:
        halo_rec[idx]=tau_i+1
    halo_rec[1:N+1,1:N+1]=np.copy(data)
    nb_loc = np.array([[0,1],[-1,1],[-1,0],[-1,-1],[0,-1],[1,-1],[1,0],[1,1]])
    nbhd = np.array([i+1,j+1])+nb_loc
    nbr=[]
    for idx in nbhd%(N+2):
        nbr.append(halo_rec[tuple(idx)])
    return nbr

path=np.zeros((T,N,N))

for t in range(T):
    for i in range(1,N-1):
        for j in range(1,N-1):
            if path[t-1,i,j] in [1,tau_i+1]:
                path[t,i,j]=tau_i+1
            if rec[t,i,j]==0:
                nbhd=nbr(rec[t],i,j)
                ctr=0
                for k in nbhd:
                    if k==0:
                        ctr+=1
                if ctr==1:
                    path[t,i,j]=1        

##c_i = plt.cm.hot_r(np.linspace(0,1,tau_i+1))
##c_r = plt.cm.copper(np.linspace(0,1,tau_r))
##c = np.vstack((c_i,c_r))
##cmap = colors.ListedColormap(c)
##b=np.arange(0.,tau_0+1,1);b[1:]-=0.01;bounds=b
##norm = colors.BoundaryNorm(bounds,cmap.N)

cmap = colors.ListedColormap(['xkcd:pale grey','xkcd:darkish red','xkcd:almost black'])
bounds = [0,0.99,tau_i+0.99,tau_0+0.99]
norm = colors.BoundaryNorm(bounds,cmap.N)

def plot(t):
    fig,ax = plt.subplots()
    ax.set_xticks(np.array(range(0,N))+0.5)
    ax.set_yticks(np.array(range(0,N))+0.5)
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.set_aspect('equal')
    ax.grid(which='both')
    ax.set_title("$t={}$".format(t))
    for i in range(N):
        for j in range(N):
            c='w' if rec[t].T[i,j]>0 else 'k'
            ax.text(i,j,'{0:1d}'.format(int(rec[t].T[i,j])),color=c,va='center',ha='center')
    ax.imshow(rec[t],origin='lower',cmap=cmap,norm=norm,interpolation='none')
    plt.show()
    return
    
def plotrange(t1,t2,step,shape):
##    fig,ax = plt.subplots(1,len(range(t1,t2,step)))
    fig,ax = plt.subplots(*shape,figsize=(shape[1]*5,shape[0]*5))
    ctr=0
    for t in range(t1,t2,step):
        ax.flat[ctr].set_xticks(np.array(range(0,N))+0.5)
        ax.flat[ctr].set_yticks(np.array(range(0,N))+0.5)
        ax.flat[ctr].set_xticklabels([])
        ax.flat[ctr].set_yticklabels([])
        ax.flat[ctr].set_aspect('equal')
        ax.flat[ctr].grid(which='both')
        ax.flat[ctr].set_title("$t={}$".format(t))
        for i in range(N):
            for j in range(N):
                c='w' if 2<rec[t].T[i,j]<9 else 'k'
                ax.flat[ctr].text(i,j,'{0:1d}'.format(int(rec[t].T[i,j])),color=c,va='center',ha='center')
        ax.flat[ctr].imshow(rec[t],origin='lower',cmap=cmap,norm=norm,interpolation='none')
        ctr+=1
    fig.tight_layout()
    plt.subplots_adjust(wspace=0.1,hspace=0.15)
##    ax[0].annotate("",xytext=(0.47,0.5),xy=(0.54,0.5),xycoords='figure fraction',
##                    arrowprops=dict(arrowstyle='simple',fc='k',ec='w',mutation_scale=20))
    plt.savefig('./fig/patch_{0}-{1}.png'.format(tau_i,tau_r))
##    plt.savefig('./fig/grad_{0}-{1}.png'.format(tau_i,tau_r))
    plt.show()
    return

def animstate(n):
    grid.set_data(rec[n]) 
    ax.set_title("$t={}$".format(n+1))
    return grid,

def animpath(n):
    ax.cla()
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_aspect('equal')
    ax.set_title("$t={}$".format(n+1))
##    for i in range(N):
##        for j in range(N):
##            c='w' if rec[n].T[i,j]>0 else 'k'
####            c='w' if 2<rec[n].T[i,j]<9 else 'k'
##            ax.text(i,j,'{0:1d}'.format(int(rec[n].T[i,j])),color=c,va='center',ha='center')
    ax.imshow(path[n],origin='lower',cmap=cmap,norm=norm,interpolation='none')
    plt.draw()
    return ax

if __name__=='__main__':

##    fig,ax = plt.subplots()
##    ani = anim.FuncAnimation(fig,animpath,T,interval=300,repeat=False)

    fig,ax = plt.subplots()
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_aspect('equal')
    ax.set_title("$t={}$".format(0))
    grid=ax.imshow(rec[0],origin='lower',cmap=cmap,norm=norm,interpolation='none')
    ani = anim.FuncAnimation(fig,animstate,T,interval=100,repeat=False)
##    ani.save('GHCA.gif',dpi=100,writer='imagemagick')

##    plotrange(0,20,6,(1,4))
    plt.show()
