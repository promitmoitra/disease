#!/bin/bash
#PBS -N ca_3-5
#PBS -q rq
#PBS -l select=1:ncpus=40
#PBS -l walltime=360:00:00
#PBS -o ./pbsOUT/
#PBS -j oe
#PBS -V

module load anaconda/3
cd $PBS_O_WORKDIR

python3 ghca_core.py
