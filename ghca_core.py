import numpy as np
import ghca_main as ca

def base_conv(number,base=2,padding=0):
    digits = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    num = abs(number)
    res = []
    while num:
        res.append(digits[int(num % base)])
        num //= base
    if padding:
        res.append('0' * padding)
    if number < 0:
        res.append('-')
    return ''.join(reversed(res or '0'))

def str_to_state(str_config,base):
    pop = []
    for s in str_config:
        pop.append(int(s,base))
    pop = np.array(pop,dtype=np.int8)
    pop = np.reshape(pop,(int(np.sqrt(len(str_config))),int(np.sqrt(len(str_config)))))
    return pop

def state_to_str(pop,base):
    str_config = ''
    pop = pop.flatten()
    for i in pop:
        str_config+=base_conv(i,base)
    return str_config

def l2norm(str1,str2,core,n_states):
    i = 0
    count = 0
    while(i < len(str1)):
        a = int(str1[i],n_states) ; b = int(str2[i],n_states)
        count += np.sqrt((a-b)**2)
        i += 1
    return count

def hamming(str1,str2,core):
    i = 0
    count = 0
    while(i < len(str1)):
        if(str1[i] != str2[i]):
            count += 1
        i += 1
    return count/core

def modulo(pop1,pop2,core,n_states):
    d = 0; pop1=pop1.flatten(); pop2=pop2.flatten()
    for i in range(core):
        d+=min(abs(pop1[i]-pop2[i]),n_states-abs(pop1[i]-pop2[i]))
    return d/core

##def gen_configs(core_size,n_states):
##    grid_size = np.sqrt(core_size) + 2
##    embedded_configs = np.empty((int(n_states**core_size),int(grid_size),int(grid_size)),dtype=np.int8)
##    np.save(antya_data+ic_path+config_filename,embedded_configs)
##    embedded_configs = np.load(antya_data+ic_path+config_filename,mmap_mode='r+',allow_pickle=True)
##    for ic in range(n_states**core_size):
##        s_str = base_conv(ic,n_states);s_str=s_str.rjust(core_size,'0')
##        p = np.zeros((int(grid_size),int(grid_size)),dtype=np.int8)
##        q = str_to_state(s_str,n_states)
##        if int(np.sqrt(core_size))%2==0:
##            p[int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2),
##            int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)]=q
##        else:
##            p[int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)+1,
##            int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)+1]=q
##
##        embedded_configs[ic]=p
##        embedded_configs.flush()
##    return
##
##def check_active_configs(core_size,active,passive,gen=False,emb=True):
##    """
##    Check asymptotic activity.
##    gen = True - Generate config from int using base conversion and check activity. More computation, less memory issues.
##    gen = False - Read config (cf. gen_config) from file and check activity.
##    emb = True - Generate or read embedded config
##    emb = False - Generate or read strictly core config  
##    """
##    n_states = active+passive+1 ; T = 2*n_states 
##    check_act = []
##
##    if not gen:
##        for ic in range(n_states**core_size):
##            if emb:
##                p = configs[ic] 
##            else:
##                q = configs[ic]; p = q[1:int(np.sqrt(core_size)+1),1:int(np.sqrt(core_size)+1)]
##
##            com = [ca.Population(p=np.copy(p),act=active,pas=passive)]
##            for t in range(T):
##                com = ca.run(com)
##        
##            state = state_to_str(com[0].p,n_states)
##            if any(base_conv(e,n_states) in state for e in range(1,active+1)):
##                check_act.append(1)
##            else:
##                check_act.append(0)
##
##    else:
##        if emb:
##            grid_size = np.sqrt(core_size) + 2
##        else:
##            grid_size = np.sqrt(core_size)
##        for ic in range(n_states**core_size):
##            s_str = base_conv(ic,n_states);s_str=s_str.rjust(core_size,'0')
##            p = np.zeros((int(grid_size),int(grid_size)),dtype=np.int8)
##            q = str_to_state(s_str,n_states)
##            if int(np.sqrt(core_size))%2==0:
##                p[int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2),
##                int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)]=q
##            else:
##                p[int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)+1,
##                int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)+1]=q
##    
##            com = [ca.Population(p=np.copy(p),act=active,pas=passive)]
##    
##            for t in range(T):
##                com = ca.run(com)
##        
##            state = state_to_str(com[0].p,n_states)
##            if any(base_conv(e,n_states) in state for e in range(1,active+1)):
##                check_act.append(1)
##            else:
##                check_act.append(0)
##
##    act_config_id = [i for i,e in enumerate(check_act) if e==1]
##    active_config_fraction = len(act_config_id)/n_states**core_size
##    return act_config_id,active_config_fraction

#######################################################################
#######################  multiprocessing_mods  ########################
#######################################################################

def emb(ic,core_size,active,passive,T):
    n_states = active+passive+1
    grid_size = np.sqrt(core_size) + 2
    s_str = base_conv(ic,n_states);s_str=s_str.rjust(core_size,'0')
    q = str_to_state(s_str,n_states)
    p = np.zeros((int(grid_size),int(grid_size)),dtype=np.int8)
    if int(np.sqrt(core_size))%2==0:
        p[int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2),
        int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)]=q
    else:
        p[int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)+1,
        int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)+1]=q

    com = [ca.Population(p=np.copy(p),act=active,pas=passive,periodic=True)]
    states = np.zeros((T,int(grid_size),int(grid_size)),dtype=np.int8)
    for t in range(T):
        states[t] = np.copy(com[0].p)
        com = ca.run(com)
    ca.animate(com,states,T=T,txt=True,interval=500)
    return

def chk(ic,core_size,active,passive,T):
    n_states = active+passive+1
    grid_size = np.sqrt(core_size) + 2
    s_str = base_conv(ic,n_states);s_str=s_str.rjust(core_size,'0')
    q = str_to_state(s_str,n_states)
    p = np.zeros((int(grid_size),int(grid_size)),dtype=np.int32)
    if int(np.sqrt(core_size))%2==0:
        p[int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2),
        int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)]=q
    else:
        p[int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)+1,
        int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)+1]=q

    com = [ca.Population(p=np.copy(p),act=active,pas=passive)]
    for t in range(T):
        com = ca.run(com)
    state = state_to_str(com[0].p,n_states)
    if any(base_conv(e,n_states) in state for e in range(1,active+1)):
        return 1
    else:
        return 0

def chk_act(core_size,active,passive):
    n_states = active+passive+1 ; n_configs = n_states**core_size
    T = 30 
    check_act = []

    with concurrent.futures.ProcessPoolExecutor() as executor:
        results = executor.map(chk,range(n_configs),repeat(core_size),repeat(active),repeat(passive),repeat(T))
        for res in results:
            check_act.append(res)

    act_config_id = [i for i,e in enumerate(check_act) if e==1]
    active_config_fraction = len(act_config_id)/n_configs
    return act_config_id,active_config_fraction

if __name__=='__main__':

    import time
    import os.path
    import sys
    import concurrent.futures
    from itertools import repeat

####==TEST==####
##    core_len = 2
##    for pair in [(1,1)]:
##        start = time.perf_counter()#monotonic()
####        act_conf_id,frac = check_active_configs(core_len**2,*pair,gen=True,emb=False)
##        act_conf_id,frac = chk_act(core_len**2,*pair)
##        end=time.perf_counter()#monotonic() 
##        print('Cycle pair:({0:02d},{1:02d})'.format(*pair),
##              '\nRuntime: ',time.strftime("%H:%M:%S",time.gmtime(end-start)),
##              '\nP(perst)  = ',frac,'\n')
####==----==####

    state_cycle = 18 ; core_len = 3

    ws_path = "/home/ramanujan/Users/promit/disease/"
    antya_run = "/scratch/scratch_run/promit.moitra/disease/"
    data_path = "result/dis_core/core-{0:02d}/".format(core_len**2)

####==CALC_PROB==####
    state_set = list(range(1,state_cycle))
##    basin_state_pairs = [(i,j) for i in state_set for j in state_set]
    basin_state_pairs = [(3,5)]#,(4,4),(5,3),(5,4),(5,5)]
##    passive = int(sys.argv[1])
##    for pair in passive:
    for pair in basin_state_pairs:
        ids_filename = "act_config_ids_states-({0:02d},{1:02d})_core-{2:02d}.npy".format(*pair,core_len**2)
        if os.path.exists(antya_run+data_path+ids_filename):
##        if os.path.exists(ws_path+data_path+ids_filename):
            print("Data exists for ({0:02d},{1:02d})\n".format(*pair))
            continue
        else:
            start = time.monotonic()
##            act_conf_id,frac = check_active_configs(core_len**2,*pair)
            act_conf_id,frac = chk_act(core_len**2,*pair)
            np.save(antya_run+data_path+'rq/'+ids_filename,act_conf_id)
##            np.save(ws_path+data_path+ids_filename,act_conf_id)
            end=time.monotonic()
            print('Cycle pair:({0:02d},{1:02d})'.format(*pair),
                    '\nRuntime: ',time.strftime("%H:%M:%S",time.gmtime(end-start)),
                    '\nP(perst) = {0:05f}\n'.format(frac))
####==---------==####
