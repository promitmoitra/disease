import numpy as np
import matplotlib.pyplot as plt

a = np.array([11,120,953,3600+(22*60)+53,5*3600+(40*60)])
b = np.array([3,4,5,6,7])

m,c = np.polyfit(b,np.log(a),1)

plt.plot(b,a,'-o',label="runtime")
plt.plot(b,np.exp(m*b+c),'-o',label="scaling fit")
plt.xlabel("states $K$");plt.ylabel("$t$")
plt.show()

