import numpy as np
from numba import njit, prange
import matplotlib.pyplot as plt
from matplotlib import colors
import matplotlib.animation as animation

#GHCA lean code, for numba optimization
#Caveats: L has to be > 1, and boundaries are periodic

##p = np.array([[0,0,0,0,0],
##              [0,3,0,2,0],
##              [0,0,1,0,0],
##              [0,0,0,0,0],
##              [0,0,0,0,0]])

T = 200; L = 100; act = 1; pas = 1 
cycle = act+pas; n_states=cycle+1
radius = 1; threshold = 1
default_nbh = np.array([np.array([x,y]) for x in range(-L+1,L)
                        for y in range(-L+1,L)
                        if np.sqrt(x**2+y**2)<=radius and [x,y]!=[0,0]])

@njit
def nbr(p,i,j):
    nbh = np.array([i,j]) + default_nbh
    nbh = nbh.flatten()
    c_nbrs = np.empty((len(default_nbh))); cntr = 0
    while cntr<len(nbh):
        c_nbrs[cntr//2] = p[nbh[cntr]%L,nbh[cntr+1]%L]
        cntr+=2
    return c_nbrs

@njit
def check_infect(p):
    next_p = np.copy(p)
    for i in range(L):
        for j in range(L):
            if p[i,j] >= cycle:
                next_p[i,j] = 0
            if 1<=p[i,j]<cycle:
                next_p[i,j] += 1
            if p[i,j] == 0:
                nbhood = nbr(p,i,j)
                if len(nbhood[np.where((nbhood>=1)&(nbhood<=act))]) >= threshold:
                    next_p[i,j] +=1
    p = np.copy(next_p)
    return p


###################################################################################################################################################

##def base_conv(number,base=2,padding=0):
##    digits = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
##    num = abs(number)
##    res = []
##    while num:
##        res.append(digits[int(num % base)])
##        num //= base
##    if padding:
##        res.append('0' * padding)
##    if number < 0:
##        res.append('-')
##    return ''.join(reversed(res or '0'))
##
##def str_to_state(str_config,base):
##    pop = []
##    for s in str_config:
##        pop.append(int(s,base))
##    pop = np.array(pop,dtype=np.int8)
##    pop = np.reshape(pop,(int(np.sqrt(len(str_config))),int(np.sqrt(len(str_config)))))
##    return pop
##
##def state_to_str(pop,base):
##    str_config = ''
##    pop = pop.flatten()
##    for i in pop:
##        str_config+=base_conv(i,base)
##    return str_config
##
##def chk(ic,core_size,active,passive,T):
##    n_states = active+passive+1
##    grid_size = np.sqrt(core_size) + 2
##    s_str = base_conv(ic,n_states);s_str=s_str.rjust(core_size,'0')
##    q = str_to_state(s_str,n_states)
##    p = np.zeros((int(grid_size),int(grid_size)),dtype=np.int32)
##    if int(np.sqrt(core_size))%2==0:
##        p[int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2),
##        int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)]=q
##    else:
##        p[int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)+1,
##        int(grid_size/2)-int(np.sqrt(core_size)/2):int(grid_size/2)+int(np.sqrt(core_size)/2)+1]=q
##
##    com = [ca.Population(p=np.copy(p),act=active,pas=passive)]
##    for t in range(T):
##        com = ca.run(com)
##    state = state_to_str(com[0].p,n_states)
##    if any(base_conv(e,n_states) in state for e in range(1,active+1)):
##        return 1
##    else:
##        return 0
##
##def chk_act(core_size,active,passive):
##    n_states = active+passive+1 ; n_configs = n_states**core_size
##    T = 30 
##    check_act = []
##
##    with concurrent.futures.ProcessPoolExecutor() as executor:
##        results = executor.map(chk,range(n_configs),repeat(core_size),repeat(active),repeat(passive),repeat(T))
##        for res in results:
##            check_act.append(res)
##
##    act_config_id = [i for i,e in enumerate(check_act) if e==1]
##    active_config_fraction = len(act_config_id)/n_configs
##    return act_config_id,active_config_fraction

if __name__=="__main__":
    rec = np.empty((T,L,L))
    for t in range(T):
        rec[t] = np.copy(p)
        p = check_infect(p)
    animate(rec,T=T)

###############################################################################################################################################

##p = np.random.randint(0,n_states,size=L*L)
##np.random.shuffle(p)
##p = p.reshape(L,L)

##def plot(p,cid=None,ax=None,cbar=False,txt=False):
##    if ax == None:
##        fig,ax = plt.subplots()
##    ax.set_xticks(np.array(range(0,L))+0.5)
##    ax.set_yticks(np.array(range(0,L))+0.5)
##    ax.set_xticklabels([])
##    ax.set_yticklabels([])
##    ax.tick_params(axis='both', which='both', length=0)
##    ax.set_aspect('equal')
####    ax.grid(which='both')
##    if cid:
##        ax.set_title("$ID={}$".format(cid))
##    if txt:
##        s_txt, = set_plot_text(com[0].p,ax,cycle)
##
##    cmap = colors.ListedColormap(['xkcd:pale grey','xkcd:darkish red','xkcd:almost black'])
##    bounds = [0,0.99,act+0.99,cycle+0.99]
##    norm = colors.BoundaryNorm(bounds,cmap.N)
##
##    img = ax.imshow(p,cmap=cmap,norm=norm,interpolation='none')
##    if cbar:
##        if cbar == 'v':
##            cbar = plt.colorbar(img,ax=ax,cmap=cmap,norm=norm,
##                        boundaries=bounds,spacing='proportional',
##                        ticks=range(n_states),orientation='vertical',
##                        drawedges=False)
##        elif cbar == 'h':
##            cbar = plt.colorbar(img,ax=ax,cmap=cmap,norm=norm,
##                        boundaries=bounds,spacing='proportional',
##                        ticks=range(n_states),orientation='horizontal',
##                        drawedges=False)
####        cbar.ax.tick_params(labelsize=20)
##        fig.tight_layout()
##        plt.show()
##    return img,
##
##def animate(data,txt=False,interval=100,T=50):
##    cmap = colors.ListedColormap(['xkcd:pale grey','xkcd:darkish red','xkcd:almost black'])
##    bounds = [0,0.99,act+0.99,cycle+0.99]
##    norm = colors.BoundaryNorm(bounds,cmap.N)        
##        
##    fig,ax = plt.subplots()
##    img, = plot(p,ax=ax)
##
##    if txt:
##        txt, = set_plot_text(data[0],ax,cycle)
##        def update(n,data):
##            img.set_data(data[n])
##            ax.set_title('$t={}$'.format(n))
##            text, = set_plot_text(data[n],ax,cycle)
##            return img,text,
##    else:
##        def update(n,data):
##            img.set_data(data[n])
##            ax.set_title('$t={}$'.format(n))
##            return img,
##    
##    cax = fig.add_axes([0.83,0.11,0.03,0.775])
####        cax = fig.add_axes([0.05,0.05,0.9,0.03])
##    cbar = plt.colorbar(img,cax=cax,cmap=cmap,norm=norm,
##                        boundaries=bounds,spacing='proportional',
##                        ticks=range(n_states),
##                        drawedges=False,orientation='vertical')        
##
##    ani = animation.FuncAnimation(fig, update, T, fargs=(data,),
##                                  interval=interval, repeat=False)
####    ani.save('ghca.gif',dpi=300,writer='imagemagick')
##    plt.show()
##    return

