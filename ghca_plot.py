import numpy as np
from matplotlib import colors
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import ghca_main as ca
import ghca_core as cacore


def run_config_id(pair,core,config_id,T=0,viz=False,
                  emb=True,p_bounds=False,radius=1,return_str=False):
    """

    """
    active = pair[0] ; passive = pair[1] ; n_states = active+passive+1
    str_state = cacore.base_conv(config_id,n_states);str_state = str_state.rjust(core,'0')
    
    if emb:
        grid_size = np.sqrt(core)+2
        p = np.zeros((int(grid_size),int(grid_size)))
        q = cacore.str_to_state(str_state,n_states)
        if int(np.sqrt(core))%2==0:
            p[int(grid_size/2)-int(np.sqrt(core)/2):int(grid_size/2)+int(np.sqrt(core)/2),
            int(grid_size/2)-int(np.sqrt(core)/2):int(grid_size/2)+int(np.sqrt(core)/2)]=q
        else:
            p[int(grid_size/2)-int(np.sqrt(core)/2):int(grid_size/2)+int(np.sqrt(core)/2)+1,
            int(grid_size/2)-int(np.sqrt(core)/2):int(grid_size/2)+int(np.sqrt(core)/2)+1]=q
    else:
        grid_size = np.sqrt(core)        
        p = cacore.str_to_state(str_state,n_states)

    com = [ca.Population(p=np.copy(p),act=active,pas=passive,periodic=p_bounds,r=radius)]

    states=np.empty((T,int(grid_size),int(grid_size)),dtype=np.int8);str_states = []
    for t in range(T):
        states[t] = np.copy(np.array(com[0].p,dtype=np.int8))        
        state = cacore.state_to_str(com[0].p,n_states)
        str_states.append(state)
        com = ca.run(com)

    if viz:
        ca.animate(com,states,T=T,interval=500,txt=True)

    if return_str:
        return str_states,p
    else:
        return states,p


def plot_distance(act,pas,core,idx):
    """

    """
    cycle = act + pas; num_states = cycle + 1
    states,config = run_config_id((active,passive),core,idx,emb=True)
    h_dist_init = [0] ; h_dist = [0]
    for i in range(1,len(states)):
        h_dist.append(cacore.modulo(states[i-1],states[i],core,num_states))    ##Distance from previous state
        h_dist_init.append(cacore.modulo(states[0],states[i],core,num_states))    ##Distance from initial state
    plt.xlim(-0.1,len(states));plt.ylim(-0.1,1.1)
    plt.xlabel("$Time\ -\ t$")
    plt.ylabel("$Hamming\ distance\ -\ D_H$")
    plt.title("$Core\ Size:\ {0},\ Cycle\ Length:\ {1},\ Config:\ {2}$".format(core,cycle,idx))
    plt.plot(range(len(states)),h_dist,'r.-',label="From prev config")
    plt.plot(range(len(states)),h_dist_init,'k.-',label="From init config")
    plt.legend(loc=4)
    plt.show()
    return h_dist,h_dist_init


def plot_perst_prob(cycle,core,flag,anchor=1,show=True):
    """
    
    """
    cycle_set = list(range(1,cycle))
    fixed_len_pairs = [(i,j) for i in cycle_set for j in cycle_set if i+j==cycle]
    fixed_passive = anchor ; inc_act_pairs = [(i,fixed_passive) for i in cycle_set]
    fixed_active = anchor ; inc_pas_pairs = [(fixed_active,i) for i in cycle_set]

    plt.ylim(-0.1,1.1)
    plt.ylabel("$P_{perst}$")

    if flag=='Fixed\ cycle':
        state_pair=fixed_len_pairs
        plt.xticks(range(len(state_pair)),labels=state_pair)
        plt.xlabel("$States\ -\ (Active,Passive)$")
        plt.title("$Core\ Size:\ {0},\ Cycle\ Length:\ {1}$".format(core,cycle))
    elif flag=='Fixed\ passive':
        state_pair=inc_act_pairs
        plt.xticks(range(len(state_pair)),labels=[i[0] for i in state_pair])
        plt.xlabel("$Active$")
        plt.title("$Core\ Size:\ {0}$".format(core))
##        plt.plot(range(len(state_pair)),[0.6]*len(state_pair),'--k')
    elif flag=='Fixed\ active':
        state_pair=inc_pas_pairs
        plt.xticks(range(len(state_pair)),labels=[i[1] for i in state_pair])
        plt.xlabel("$Passive$")
        plt.title("$Core\ Size:\ {0}$".format(core))
    else:
        print("Incorrect flag!")

    act_frac = {}
    for pair in state_pair:
##        try:
##        act_config_ids = np.load(ws_dir+"act_config_ids_states-({0:02d},{1:02d})_core-{2:02d}.npy".format(*pair,core))
        act_config_ids = np.load(ant_dir+"core-{2:02d}/act_config_ids_states-({0:02d},{1:02d})_core-{2:02d}.npy".format(*pair,core),mmap_mode='r')
        frac = len(act_config_ids)/(sum(pair)+1)**core
        act_frac[pair]=frac
##        except FileNotFoundError:
##            print("Data for ({0},{1}) NOT available!".format(*pair))
##            act_frac[pair]=-1.0

    act_conf_frac = list(act_frac.values())
    plt.plot(range(len(state_pair)),act_conf_frac,'-o',label="${0:01d},\ Core:{1:01d}$".format(anchor,core))
    plt.legend(loc='best',title="${}$".format(flag))
    if show:
        plt.show()
    return act_frac


def plot_basin(core):
    """
    Fraction of init configs that persist (for given core size) = basin[passive-1,active-1]

    """
    max_cyc = 6 
    len_set = list(range(1,max_cyc))
    state_pairs=[(i,j) for i in len_set for j in len_set]#;state_pairs.pop(-1)
    basin = np.zeros((max_cyc-1,max_cyc-1))
    for pair in state_pairs:
        act = pair[0];pas=pair[1]
##        conf_id = np.load(ws_dir+"act_config_ids_states-({0:02d},{1:02d})_core-{2:02d}.npy".format(act,pas,core))
        try:
            conf_id = np.load(ant_dir+"core-{2:02d}/act_config_ids_states-({0:02d},{1:02d})_core-{2:02d}.npy".format(act,pas,core),mmap_mode='r')
            basin[pas-1,act-1] = len(conf_id)/(np.sum(pair)+1)**core
        except FileNotFoundError:
            basin[pas-1,act-1] = 0
    plt.xticks(range(max_cyc-1),labels=len_set);plt.yticks(range(max_cyc-1),labels=len_set)
    plt.xlabel("$Active$");plt.ylabel("$Passive$")
    plt.imshow(basin,cmap='jet',origin='lower',interpolation='none')
    plt.title("$Core\ Size:\ {0}$".format(core))
    plt.colorbar()
    plt.show()
    return basin


def plotrange(init,fin,step,shape,rec):
    """

    """
##    core_len = int(np.sqrt(len(rec[0])))+2
    fig,ax = plt.subplots(*shape,figsize=(shape[1]*5,shape[0]*5))
    ctr=0
    for idx in range(init,fin,step):
        ID = list(act_config_dict.keys())[idx]
        com = [ca.Population(p=np.copy(rec[idx]),act=active,pas=passive)]
        img = ca.plot(com,ID,ax=ax.flat[ctr],txt='True')
        ctr+=1
##    cax = fig.add_axes([0.9,0.11,0.01,0.79])
    cax = fig.add_axes([0.05,0.05,0.9,0.03])
    cbar = plt.colorbar(img[0],cax=cax,cmap=cmap,norm=norm,
                        boundaries=bounds,spacing='proportional',
                        ticks=range(num_states),
                        drawedges=False,orientation='horizontal')
##    cbar.ax.tick_params(labelsize=20)
    plt.subplots_adjust(top=0.9,wspace=0.15,hspace=0.0)
    fig.suptitle("$Core:\ {}\ \ Cycle:\ ({},{})$".format(core,active,passive))
##    plt.savefig('{}.png'.format())
    fig.tight_layout()
    plt.show()
    return


def extract_cycle(str_states,n_states):
    ids=[]
    for i in str_states:
        ids.append(int(i,n_states))
    check=[]
    for i in range(len(ids)):
        if ids[i] in check:
            #Using the fact that list.index() returns the index of the first occurrence of any element
            lim_cyc=ids[ids.index(ids[i]):i]
            break
        check.append(ids[i])
    return lim_cyc


if __name__=='__main__':

    active=5; passive=1; cycle=active+passive; num_states=cycle+1
    core_len = 3; core_size = np.square(core_len)
    emb_core_len = core_len+2; emb_core_size=np.square(emb_core_len)
    idx = 23
    
    cmap = colors.ListedColormap(['xkcd:pale grey','xkcd:darkish red','xkcd:almost black'])
    bounds = [0,0.5,active+0.5,cycle+0.5]
    norm = colors.BoundaryNorm(bounds,cmap.N)

    ant_dir = "./result/dis_core/"#core-{0:02d}/".format(core_size)
    ws_dir = "../antya_data/disease/result/dis_core/core-{0:02d}/".format(core_size)

##    config_ids = np.load(ant_dir+"test/"+"act_config_ids_states-({0:02d},{1:02d})_core-{2:02d}.npy".format(active,passive,core_size),mmap_mode='r')
##    print('Total configs =',num_states**core_size,'\nActive configs = ',len(config_ids))

##    act_config_dict={};limit_cycles=[]
##    for cid in config_ids:
##        s,p=run_config_id((active,passive),core_size,cid,T=0,emb=True,return_str=True)
##        act_config_dict[cid] = p
##        lc = extract_cycle(s,num_states)

##       flag = 1
##       for i in limit_cycles:
##           if tuple(sorted(i)) == tuple(sorted(lc)):
##               flag=0
##               break
##       if flag:
##           limit_cycles.append(tuple(lc))

##    s,p = run_config_id((active,passive),emb_core_size,1,T=50,emb=False,viz=True,return_str=True)

##    com = [ca.Population(p=np.copy(act_config_dict[config_ids[0]]),
##                         act=active,pas=passive)]
##    ca.plot(com,cid=config_ids[0],cbar='v',txt='True')
##
##    plotrange(0,46,1,(2,23),list(act_config_dict.values()))
##
#    s,p=run_config_id((active,passive),core_size,2,T=100,viz=True)

##########!!!!Refine definition of hamming(a,b) in dis_core_ant!!!!##########    
##    h_d,h_d_init = plot_distance(active,passive,core_size,idx)
#############################################################################

##    basin = plot_basin(core_size)

##    flag='Fixed\ cycle'
##    act_frac = plot_perst_prob(cycle,core_size,flag)

    flag='Fixed\ active';anch = 1
    core_size = 4
##    act_frac = plot_perst_prob(cycle,core_size,flag,anch,show=False)
    b4 = plot_basin(core_size)
    core_size = 9
##    act_frac = plot_perst_prob(cycle,core_size,flag,anch)
    b9 = plot_basin(core_size)

##    for i in range(1,cycle):
####        flag='Fixed\ active'
##        flag='Fixed\ passive'
##        active_fraction = plot_perst_prob(cycle,core_size,flag,i,show=False)
##        plt.legend(loc='best',title="${}$".format(flag))
##    plt.show()
