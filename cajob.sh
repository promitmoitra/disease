#!/bin/bash
#PBS -N c-09_4-i
#PBS -q serialq
#PBS -l select=1:ncpus=10
#PBS -l walltime=40:00:00
#PBS -o ./pbsOUT/
#PBS -j oe
#PBS -V
#PBS -J 1-5

module load anaconda/3
cd $PBS_O_WORKDIR

python ghca_core.py $PBS_ARRAY_INDEX
